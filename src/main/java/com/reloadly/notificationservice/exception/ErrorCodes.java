package com.reloadly.notificationservice.exception;

public class ErrorCodes {
    //Field Errors
    public static final String INVALID_FIELD = "INVALID_FIELD";
    public static final String MISSING_PATH = "MISSING_PATH";
    public static final String MISSING_REQUEST_PARAM = "MISSING_REQUEST_PARAM";
}
