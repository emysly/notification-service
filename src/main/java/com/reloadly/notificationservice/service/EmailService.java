package com.reloadly.notificationservice.service;

import com.reloadly.notificationservice.http.EmailRequest;

public interface EmailService {
    String sendEmail(EmailRequest emailRequest);
}
