package com.reloadly.notificationservice.service.impl;

import com.reloadly.notificationservice.config.EmailConfig;
import com.reloadly.notificationservice.http.EmailRequest;
import com.reloadly.notificationservice.service.EmailService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender sender;
    private final EmailConfig emailConfig;

    @Override
    public String sendEmail(EmailRequest emailRequest) {

        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(emailRequest.getTo());
            message.setFrom(emailConfig.getFrom());
            message.setText(emailRequest.getText());
            message.setSubject(emailRequest.getSubject());

            sender.send(message);
            log.info("Mail Sent");
            return "Mail Sent";
        } catch (Exception ex) {
            log.error("Messaging Error: ", ex);
            throw new MailParseException("Email not sent");
        }

    }
}
